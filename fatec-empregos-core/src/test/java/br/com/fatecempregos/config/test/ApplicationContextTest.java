/**
 * 
 */
package br.com.fatecempregos.config.test;

import java.util.Properties;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.jolbox.bonecp.BoneCPDataSource;

/**
 * Class com todas configurações necessárias para inicialização do contexto de aplicação do Spring
 *
 * @author tiarebalbi
 * @date Apr 12, 2013
 * @package br.com.fatecempregos.configuracao
 */
@ComponentScan(basePackages = {
		"br.com.fatecempregos.component",
		"br.com.fatecempregos.service"
})
@EnableTransactionManagement
@EnableJpaRepositories("br.com.fatecempregos.repository")
@PropertySource("classpath:definicoes-test.properties")
@Configuration
public class ApplicationContextTest {
	
	@Resource
    private Environment environment;
	
	private static final String NOME_PROPRIEDADE_DATABASE_DRIVER = "db.driver";
    private static final String NOME_PROPRIEDADE_DATABASE_PASSWORD = "db.password";
    private static final String NOME_PROPRIEDADE_DATABASE_URL = "db.url";
    private static final String NOME_PROPRIEDADE_DATABASE_USERNAME = "db.username";

    private static final String NOME_PROPRIEDADE_HIBERNATE_DIALECT = "hibernate.dialect";
    private static final String NOME_PROPRIEDADE_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    private static final String NOME_PROPRIEDADE_HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";
    private static final String NOME_PROPRIEDADE_HIBERNATE_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";
    private static final String NOME_PROPRIEDADE_HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private static final String NOME_PROPRIEDADE_ENTITYMANAGER_PACKAGES_TO_SCAN = "entitymanager.packages.to.scan";
	private static final String NOME_PROPRIEDADE_HIBERNATE_CACHE = "hibernate.cache.provider_class";
	
	/**
	 * Método para configuração do pool de conexão (BoneCP)
	 * 
	 * @return {@link DataSource}
	 */
	@Bean
    public DataSource dataSource() {
        BoneCPDataSource dataSource = new BoneCPDataSource();

        dataSource.setDriverClass(environment.getRequiredProperty(NOME_PROPRIEDADE_DATABASE_DRIVER));
        dataSource.setJdbcUrl(environment.getRequiredProperty(NOME_PROPRIEDADE_DATABASE_URL));
        dataSource.setUsername(environment.getRequiredProperty(NOME_PROPRIEDADE_DATABASE_USERNAME));
        dataSource.setPassword(environment.getRequiredProperty(NOME_PROPRIEDADE_DATABASE_PASSWORD));
        
        return dataSource;
    }
	
    /**
     * Gerenciador de Transações
     * 
     * @return {@link JpaTransactionManager}
     * @throws ClassNotFoundException
     */
    @Bean
    public JpaTransactionManager transactionManager() throws ClassNotFoundException {
        JpaTransactionManager transactionManager = new JpaTransactionManager();

        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }
    
    
    /**
     * Configuração do Entity Manager
     * @return {@link LocalContainerEntityManagerFactoryBean}
     * @throws ClassNotFoundException
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws ClassNotFoundException {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan(environment.getRequiredProperty(NOME_PROPRIEDADE_ENTITYMANAGER_PACKAGES_TO_SCAN));

        Properties jpaProterties = new Properties();
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_DIALECT, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_DIALECT));
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_FORMAT_SQL, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_FORMAT_SQL));
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_HBM2DDL_AUTO, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_HBM2DDL_AUTO));
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_NAMING_STRATEGY, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_NAMING_STRATEGY));
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_SHOW_SQL, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_SHOW_SQL));
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_CACHE, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_CACHE));

        entityManagerFactoryBean.setJpaProperties(jpaProterties);

        return entityManagerFactoryBean;
    }

}
