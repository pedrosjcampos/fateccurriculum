/**
 * 
 */
package br.com.fatecempregos.repository.test;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import br.com.fatecempregos.common.FatecEmpregosException;
import br.com.fatecempregos.config.test.ApplicationContextTest;
import br.com.fatecempregos.model.Usuario;
import br.com.fatecempregos.predicate.UsuarioPredicate;
import br.com.fatecempregos.repository.UsuarioRepository;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

/**
 * @author tiarebalbi
 * @date Apr 18, 2013
 * @package br.com.fatecempregos.repository.test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=ApplicationContextTest.class)
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class })
@Transactional
public class UsuarioRepositoryTest {
	
	@Resource
	private UsuarioRepository repository;

	private Predicate condicaoStatusFalse;

	private Predicate condicaoStatusTrue;
	
	private OrderSpecifier<String> ordem;

	@Test
	public void deveModeloUmUsuario() throws Exception {
		Usuario entidade = new Usuario();
		entidade.setUsername("login");
		entidade.setSenha("senha");
		entidade.setVersion(0);
		Usuario retorno = this.repository.salvar(entidade);
		Assert.assertEquals(retorno.getUsername(), entidade.getUsername());
	}
	
	@Test
	public void deveBuscarUmUsuario() throws FatecEmpregosException {
		
		Usuario entidade = new Usuario();
		entidade.setUsername("rod");
		entidade.setSenha("koala");
		entidade.setVersion(0);
		
		Usuario salvo = this.repository.salvar(entidade);
		
		Usuario retorno = this.repository.findRow(UsuarioPredicate.buscarUsuarioPorLogin(salvo.getUsername()));
		Assert.assertEquals(salvo.getUsername(), retorno.getUsername());
		
	}
	
	
}
