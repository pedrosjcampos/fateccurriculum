package br.com.fatecempregos.common;

public class FatecEmpregosException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3777675802670810456L;

	
	public FatecEmpregosException() {
		super();
	}
	
	public FatecEmpregosException(String mensagem) {
		super(mensagem);
	}
	
}
