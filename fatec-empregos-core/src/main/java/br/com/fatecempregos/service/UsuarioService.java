package br.com.fatecempregos.service;

import java.util.List;

import br.com.fatecempregos.common.FatecEmpregosException;
import br.com.fatecempregos.model.Usuario;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

public interface UsuarioService {
	
	Usuario save(Usuario entidade) throws FatecEmpregosException;
	
	List<Usuario> findAll();
	
	List<Usuario> findAll(Predicate condicao);
	
	List<Usuario> findAll(Predicate condicao, OrderSpecifier<?> ordem);

	Usuario delete(Long id);
	
	Usuario findRow(Long id);
	
	Usuario findRow(Predicate condicao);

}
