package br.com.fatecempregos.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

import br.com.fatecempregos.common.FatecEmpregosException;
import br.com.fatecempregos.model.Usuario;
import br.com.fatecempregos.repository.UsuarioRepository;
import br.com.fatecempregos.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Resource 
	private UsuarioRepository usuarioRepository;
	
	@Override
	@Transactional(rollbackFor=FatecEmpregosException.class, propagation=Propagation.REQUIRED)
	public Usuario save(Usuario entidade) throws FatecEmpregosException {
		return usuarioRepository.salvar(entidade);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public List<Usuario> findAll(Predicate condicao) {
		return usuarioRepository.findAll(condicao);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Usuario> findAll(Predicate condicao, OrderSpecifier<?> ordem) {
		return usuarioRepository.findAll(condicao, ordem);
	}

	@Override
	@Transactional(rollbackFor=FatecEmpregosException.class, propagation=Propagation.REQUIRED)
	public Usuario delete(Long id) {
		return usuarioRepository.excluir(id);
	}

	@Override
	@Transactional(readOnly=true)
	public Usuario findRow(Long id) {
		return usuarioRepository.findRow(id);
	}

	@Override
	@Transactional(readOnly=true)
	public Usuario findRow(Predicate condicao) {
		return usuarioRepository.findRow(condicao);
	}
	
	

}
