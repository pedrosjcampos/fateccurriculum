package br.com.fatecempregos.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.BatchSize;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * Modelo da entidade Usuário
 * @author danilo
 * @date May 17, 2013
 * @package br.com.fatecempregos.model
 *
 */
@Entity
@BatchSize(size=10)
public class Usuario extends BaseEntity<Long>{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="nome", nullable=false)
	@NotEmpty
	private String username;
	
	@Column(name="senha", nullable=false)
	@NotEmpty
	private String senha;
	
	/**
	 * @return the id
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * @param senha the senha to set
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
}
