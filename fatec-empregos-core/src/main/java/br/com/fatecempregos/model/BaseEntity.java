/**
 * 
 */
package br.com.fatecempregos.model;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * Métodos padrões dentro de uma entidade;
 *
 * @author tiarebalbi
 * @param <ID> 
 * @date Apr 12, 2013
 * @package br.com.fatecempregos.model
 */
@MappedSuperclass
public abstract class BaseEntity<ID> {

	/**
	 * Responsável pelo LockTable em registros
	 */
	@Version
	public Integer version;
	
	/**
	 * @return ID Chave primária do Registro
	 */
	public abstract ID getId();

	/**
	 * @return método getter version
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * @param version do version método setter
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}
	
	
}
