package br.com.fatecempregos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import br.com.fatecempregos.model.Usuario;
import br.com.fatecempregos.repository.paginating.PaginatingUsuarioRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>, PaginatingUsuarioRepository , QueryDslPredicateExecutor<Usuario> {

}
