package br.com.fatecempregos.repository.paginating;

import java.util.List;

import br.com.fatecempregos.common.FatecEmpregosException;
import br.com.fatecempregos.model.Usuario;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

public interface PaginatingUsuarioRepository {
	
	Usuario salvar(Usuario entidade) throws FatecEmpregosException;
	
	List<Usuario> findAll();
	
	List<Usuario> findAll(Predicate condicao);
	
	List<Usuario> findAll(Predicate condicao, OrderSpecifier<?> ordem);

	Usuario excluir(Long id);
	
	Usuario findRow(Long id);
	
	Usuario findRow(Predicate condicao);
	
}
