package br.com.fatecempregos.repository.impl;

import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaMetamodelEntityInformation;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.stereotype.Repository;

import br.com.fatecempregos.common.FatecEmpregosException;
import br.com.fatecempregos.model.Usuario;
import br.com.fatecempregos.repository.paginating.PaginatingUsuarioRepository;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Predicate;

@Repository
public class UsuarioRepositoryImpl implements PaginatingUsuarioRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioRepositoryImpl.class);

	@PersistenceContext
	protected EntityManager entidade;

	private QueryDslJpaRepository<Usuario, Long> repository;

	/**
	 * PostContruct
	 */
	@PostConstruct
	public void init() {
		LOGGER.info("[{}] - Iniciando configuração do Repository", Calendar.getInstance().getTime());
		
		JpaEntityInformation<Usuario, Long> entidadeInfo = new JpaMetamodelEntityInformation<Usuario, Long>(Usuario.class, this.entidade.getMetamodel());
		repository = new QueryDslJpaRepository<Usuario, Long>(entidadeInfo,this.entidade);
	}

	@Override
	public Usuario salvar(Usuario entidade) throws FatecEmpregosException {
		return this.repository.save(entidade);
	}

	@Override
	public List<Usuario> findAll() {
		return this.repository.findAll();
	}

	@Override
	public List<Usuario> findAll(Predicate condicao) {
		return this.repository.findAll(condicao);
	}

	@Override
	public List<Usuario> findAll(Predicate condicao, OrderSpecifier<?> ordem) {
		return this.repository.findAll(condicao, ordem);
	}

	@Override
	public Usuario excluir(Long id) {
		Usuario retorno = this.findRow(id);
		this.repository.delete(retorno.getId());
		return retorno;
	}

	@Override
	public Usuario findRow(Long id) {
		return this.repository.findOne(id);
	}

	@Override
	public Usuario findRow(Predicate condicao) {
		return this.repository.findOne(condicao);
	}

}
