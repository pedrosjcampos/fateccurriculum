package br.com.fatecempregos.predicate;

import br.com.fatecempregos.model.QUsuario;

import com.mysema.query.types.Predicate;

public class UsuarioPredicate {

	public static Predicate buscarUsuarioPorLogin(String login) {
		QUsuario predicate = QUsuario.usuario;
		return predicate.username.eq(login);
	}
}
