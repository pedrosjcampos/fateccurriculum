<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<spring:url value="/" var="path" />

<div class="content">
	<form class="form-vertical" action="${path}authentication/login" method="post">
		<h3 class="form-title">Login</h3>
		<div class="alert alert-error hide">
			<button class="close" data-dismiss="alert"></button>
			<span>Preencha o seu login e senha.</span>
		</div>
		<c:if test="${SPRING_SECURITY_LAST_EXCEPTION != null}">
			<div class="alert alert-error">
	           <button class="close" data-dismiss="alert"></button>
	            <strong>${SPRING_SECURITY_LAST_EXCEPTION.getMessage()}</strong>
	        </div>
	        <c:remove scope="session" var="SPRING_SECURITY_LAST_EXCEPTION"/>
        </c:if>
		<div class="control-group">
			<label class="control-label visible-ie8 visible-ie9">Usuário:</label>
			<div class="controls">
				<div class="input-icon left">
					<i class="icon-user"></i> <input class="m-wrap placeholder-no-fix required" type="text" placeholder="Usuário/E-Mail" name="username" />
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label visible-ie8 visible-ie9">Senha:</label>
			<div class="controls">
				<div class="input-icon left">
					<i class="icon-lock"></i> <input class="m-wrap placeholder-no-fix required"
						type="password" placeholder="Password" name="password" />
				</div>
			</div>
		</div>
		<a><button class="btn btn-block btn-login-flat">Entrar<i class="m-icon-swapright m-icon-white pull-right"></i></button></a>
	</form>
</div>