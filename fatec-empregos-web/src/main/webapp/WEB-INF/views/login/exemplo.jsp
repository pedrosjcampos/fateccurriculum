<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<spring:url value="/" var="path" />

<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar nav-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<ul>
			<li>
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler hidden-phone"></div> <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			</li>
			<li>
				<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
				<form class="sidebar-search">
					<div class="input-box">
						<a href="javascript:;" class="remove"></a> <input type="text"
							placeholder="Search..." /> <input type="button" class="submit"
							value=" " />
					</div>
				</form> <!-- END RESPONSIVE QUICK SEARCH FORM -->
			</li>
<%-- 			<sec:authorize access="hasRole('ROLE_ADMINISTRADOR')"> --%>
				<li><a href="${path}j_spring_security_logout">Logout</a></li>
<%-- 			</sec:authorize> --%>
			<li class="start "><a href="index.html"> <i
					class="icon-home"></i> <span class="title">Dashboard</span>
			</a></li>
			<li class="has-sub "><a href="javascript:;"> <i
					class="icon-bookmark-empty"></i> <span class="title">UI
						Features</span> <span class="arrow "></span>
			</a>
				<ul class="sub">
					<li><a href="ui_general.html">General</a></li>
					<li><a href="ui_buttons.html">Buttons</a></li>
					<li><a href="ui_tabs_accordions.html">Tabs & Accordions</a></li>
					<li><a href="ui_sliders.html">Sliders</a></li>
					<li><a href="ui_tiles.html">Tiles</a></li>
					<li><a href="ui_typography.html">Typography</a></li>
					<li><a href="ui_tree.html">Tree View</a></li>
					<li><a href="ui_nestable.html">Nestable List</a></li>
				</ul></li>
			<li class="has-sub "><a href="javascript:;"> <i
					class="icon-table"></i> <span class="title">Form Stuff</span> <span
					class="arrow "></span>
			</a>
				<ul class="sub">
					<li><a href="form_layout.html">Form Layouts</a></li>
					<li><a href="form_samples.html">Advance Form Samples</a></li>
					<li><a href="form_component.html">Form Components</a></li>
					<li><a href="form_wizard.html">Form Wizard</a></li>
					<li><a href="form_validation.html">Form Validation</a></li>
					<li><a href="form_fileupload.html">Multiple File Upload</a></li>
					<li><a href="form_dropzone.html">Dropzone File Upload</a></li>
				</ul></li>
			<li class="has-sub "><a href="javascript:;"> <i
					class="icon-th-list"></i> <span class="title">Data Tables</span> <span
					class="arrow "></span>
			</a>
				<ul class="sub">
					<li><a href="table_basic.html">Basic Tables</a></li>
					<li><a href="table_managed.html">Managed Tables</a></li>
					<li><a href="table_editable.html">Editable Tables</a></li>
				</ul></li>
			<li class="has-sub "><a href="javascript:;"> <i
					class="icon-th-list"></i> <span class="title">Portlets</span> <span
					class="arrow "></span>
			</a>
				<ul class="sub">
					<li><a href="portlet_general.html">General Portlets</a></li>
					<li><a href="portlet_draggable.html">Draggable Portlets</a></li>
				</ul></li>
			<li class="has-sub "><a href="javascript:;"> <i
					class="icon-map-marker"></i> <span class="title">Maps</span> <span
					class="arrow "></span>
			</a>
				<ul class="sub">
					<li><a href="maps_google.html">Google Maps</a></li>
					<li><a href="maps_vector.html">Vector Maps</a></li>
				</ul></li>
			<li class=""><a href="charts.html"> <i
					class="icon-bar-chart"></i> <span class="title">Visual
						Charts</span>
			</a></li>
			<li class=""><a href="calendar.html"> <i
					class="icon-calendar"></i> <span class="title">Calendar</span>
			</a></li>
			<li class=""><a href="gallery.html"> <i class="icon-camera"></i>
					<span class="title">Gallery</span>
			</a></li>
			<li class="active has-sub "><a href="javascript:;"> <i
					class="icon-briefcase"></i> <span class="title">Extra</span> <span
					class="selected"></span> <span class="arrow open"></span>
			</a>
				<ul class="sub">
					<li><a href="extra_profile.html">User Profile</a></li>
					<li><a href="extra_faq.html">FAQ</a></li>
					<li><a href="extra_search.html">Search Results</a></li>
					<li><a href="extra_invoice.html">Invoice</a></li>
					<li><a href="extra_pricing_table.html">Pricing Tables</a></li>
					<li><a href="extra_404.html">404 Page</a></li>
					<li><a href="extra_500.html">500 Page</a></li>
					<li class="active"><a href="extra_blank.html">Blank Page</a></li>
					<li><a href="extra_full_width.html">Full Width Page</a></li>
				</ul></li>
			<li class=""><a href="login.html"> <i class="icon-user"></i>
					<span class="title">Login Page</span>
			</a></li>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN PAGE -->
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<div id="portlet-config" class="modal hide">
			<div class="modal-header">
				<button data-dismiss="modal" class="close" type="button"></button>
				<h3>portlet Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here will be a configuration form</p>
			</div>
		</div>
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<!-- BEGIN PAGE CONTAINER-->
		<div class="container-fluid">
			<!-- BEGIN PAGE HEADER-->
			<div class="row-fluid">
				<div class="span12">
					<!-- BEGIN STYLE CUSTOMIZER -->
					<div class="color-panel hidden-phone">
						<div class="color-mode-icons icon-color"></div>
						<div class="color-mode-icons icon-color-close"></div>
						<div class="color-mode">
							<p>THEME COLOR</p>
							<ul class="inline">
								<li class="color-black current color-default"
									data-style="default"></li>
								<li class="color-blue" data-style="blue"></li>
								<li class="color-brown" data-style="brown"></li>
								<li class="color-purple" data-style="purple"></li>
								<li class="color-white color-light" data-style="light"></li>
							</ul>
							<label class="hidden-phone"> <input type="checkbox"
								class="header" checked value="" /> <span
								class="color-mode-label">Fixed Header</span>
							</label>
						</div>
					</div>
					<!-- END BEGIN STYLE CUSTOMIZER -->
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
						Blank Page <small>blank & starter page sample</small>
					</h3>
					<ul class="breadcrumb">
						<li><i class="icon-home"></i> <a href="index.html">Home</a> <i
							class="icon-angle-right"></i></li>
						<li><a href="#">Extra</a> <i class="icon-angle-right"></i></li>
						<li><a href="#">Blank Page</a></li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row-fluid">
				<div class="span12">Blank page content goes here</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE -->
</div>
