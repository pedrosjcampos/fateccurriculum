<div class="page-content">
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12">
				<h3 class="page-title">
					Usu�rios <small>Gerenciamento de usu�rios</small>
				</h3>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption"><i class="icon-user"> </i>Usu�rios</div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Nome</th>
									<th>Last Name</th>
									<th class="hidden-480">Username</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Mark</td>
									<td>Otto</td>
									<td class="hidden-480">makr124</td>
								</tr>
								<tr>
									<td>2</td>
									<td>Jacob</td>
									<td>Nilson</td>
									<td class="hidden-480">jac123</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Larry</td>
									<td>Cooper</td>
									<td class="hidden-480">lar</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Sandy</td>
									<td>Lim</td>
									<td class="hidden-480">sanlim</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
