<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<spring:url value="/" var="path" />
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
  	<meta charset="utf-8" />
  	<title><tiles:insertAttribute name="prefixTitle" /><tiles:insertAttribute name="title" /></title>
 	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
 	<meta content="" name="description" />
  	<meta content="" name="author" />
  	<link href="${path}assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  	<link href="${path}assets/css/metro.css" rel="stylesheet" />
  	<link href="${path}assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  	<link href="${path}assets/css/style.css" rel="stylesheet" />
  	<link href="${path}assets/css/style_responsive.css" rel="stylesheet" />
  	<link href="${path}assets/css/style_default.css" rel="stylesheet" id="style_color" />
  	<link rel="stylesheet" type="text/css" href="${path}assets/uniform/css/uniform.default.css" />
  
  	<!--  CSS Padrão -->
  	<tiles:importAttribute name="style"/>
	<c:forEach items="${style}" var="css">
		<link rel="stylesheet" href="${path}ease/interno/css/${css}">
	</c:forEach>
	<!-- Javascript interno das páginas -->
	<tiles:importAttribute name="javascript"/>
	<c:forEach items="${javascript}" var="js">
		<script src="${path}ease/interno/js/${js}"></script>
	</c:forEach>
</head>
<body class="login login-flat">
  <div class="logo">
<!--     <img src="assets/img/logo-big.png" alt="" />  -->
<!-- No local colocar a futura logo -->
  </div>

	<!-- Login onde o conteúdo será carregado -->
	<tiles:insertAttribute name="content"></tiles:insertAttribute>

  <script src="${path}assets/js/jquery-1.8.3.min.js"></script>
  <script src="${path}assets/bootstrap/js/bootstrap.min.js"></script>  
  <script src="${path}assets/uniform/jquery.uniform.min.js"></script> 
  <script src="${path}assets/js/jquery.blockui.js"></script>
  <script type="text/javascript" src="${path}assets/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="${path}assets/js/app.js"></script>
  <script>
    jQuery(document).ready(function() {     
      App.initLogin();
    });
  </script>
</body>
</html>