<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<spring:url value="/" var="path" />
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title><tiles:insertAttribute name="prefixTitle" />
	<tiles:insertAttribute name="title" /></title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<link href="${path}assets/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="${path}assets/css/metro.css" rel="stylesheet" />
<link href="${path}assets/bootstrap/css/bootstrap-responsive.min.css"
	rel="stylesheet" />
<link href="${path}assets/font-awesome/css/font-awesome.css"
	rel="stylesheet" />
<link
	href="${path}assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"
	rel="stylesheet" />
<link href="${path}assets/css/style.css" rel="stylesheet" />
<link href="${path}assets/css/style_responsive.css" rel="stylesheet" />
<link href="${path}assets/css/style_default.css" rel="stylesheet"
	id="style_color" />
<link rel="stylesheet" type="text/css"
	href="${path}assets/chosen-bootstrap/chosen/chosen.css" />
<link rel="stylesheet" type="text/css"
	href="${path}assets/uniform/css/uniform.default.css" />

<tiles:importAttribute name="style" />
<c:forEach items="${style}" var="css">
	<link rel="stylesheet" href="${path}ease/interno/css/${css}">
</c:forEach>

<tiles:importAttribute name="javascript" />
<c:forEach items="${javascript}" var="js">
	<script src="${path}ease/interno/js/${js}"></script>
</c:forEach>

</head>
<body class="fixed-top">
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a href="javascript:;" class="btn-navbar collapsed"
					data-toggle="collapse" data-target=".nav-collapse"> <img
					src="assets/img/menu-toggler.png" alt="" />
				</a>
			</div>
		</div>
	</div>
	<div class="page-container row-fluid">
		<div class="page-sidebar nav-collapse collapse">
			<ul>
				<li>
					<div class="sidebar-toggler hidden-phone"></div>
				</li>

				<li class="start"><a href="${path}logout">
						<i class="icon-signout"></i> <span class="title">Sair</span>
				</a></li>

				<li class=""><a href="${path}home"> <i class="icon-home"></i>
						<span class="title">Página inicial</span>
				</a></li>

				<li class="has-sub "><a href="javascript:;"> <i
						class="icon-cog"></i> <span class="title">Administração</span>
						<span class="arrow "></span>
				</a>
					<ul class="sub">
						<li><a href="${path}usuarios">Alunos</a></li>
					</ul></li>
			</ul>
		</div>
		<tiles:insertAttribute name="content"></tiles:insertAttribute>
	</div>


<!-- 	<div class="footer"> -->
<!-- 		2013 &copy; Todos os direitos reservados -->
<!-- 		<div class="span pull-right"> -->
<!-- 			<span class="go-top"><i class="icon-angle-up"></i></span> -->
<!-- 		</div> -->
<!-- 	</div> -->

	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="${path}assets/js/jquery-1.8.3.min.js"></script>
	<script src="${path}assets/breakpoints/breakpoints.js"></script>
	<script
		src="${path}assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="${path}assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="${path}assets/js/jquery.blockui.js"></script>
	<script src="${path}assets/js/jquery.cookie.js"></script>
	<script
		src="${path}assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
	<script type="text/javascript"
		src="${path}assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript"
		src="${path}assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="${path}assets/js/excanvas.js"></script>
	<script src="${path}assets/js/respond.js"></script>
	<![endif]-->
	<script src="${path}assets/js/app.js"></script>
	<script>
		jQuery(document).ready(function() {
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();
		});
	</script>
</body>
</html>