package br.com.fatecempregos.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.fatecempregos.model.Usuario;

@Controller
@RequestMapping(value = "/usuarios")
public class UserController {

	private static final String VIEW_INDEX = "usuarios/index";

	@RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest req, HttpServletResponse resp) {
		ModelAndView view = new ModelAndView(VIEW_INDEX);
		return view;
	}
}
