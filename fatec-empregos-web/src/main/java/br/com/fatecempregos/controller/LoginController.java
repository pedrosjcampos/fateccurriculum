package br.com.fatecempregos.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController {

	
	private static final String VIEW_INDEX = "login/index";
	
	private static final String EXEMPLO_INDEX = "exemplo/index";
	

	/**
	 * Realiza uma requisição para a página inicial de login 
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @return {@link ModelAndView}
	 */
	@RequestMapping(value= "/login", method= RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView view = new ModelAndView(VIEW_INDEX);
		return view;
	}
	
	@RequestMapping(value= "/exemplo", method=RequestMethod.GET)
	public ModelAndView home(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView view = new ModelAndView(EXEMPLO_INDEX);
		return view;
	}
}
