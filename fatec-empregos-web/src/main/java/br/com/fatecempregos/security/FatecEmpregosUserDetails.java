package br.com.fatecempregos.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import br.com.fatecempregos.model.Usuario;


public class FatecEmpregosUserDetails implements org.springframework.security.core.userdetails.UserDetails {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Usuario user;

	
	public FatecEmpregosUserDetails(Usuario user){
		super();
		this.user = user;
		
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPassword() {
		return this.user.getSenha();
	}

	@Override
	public String getUsername() {
		return this.user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @return the user
	 */
	public Usuario getUser() {
		return user;
	}
}
