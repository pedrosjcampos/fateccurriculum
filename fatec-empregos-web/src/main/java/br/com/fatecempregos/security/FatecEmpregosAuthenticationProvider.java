package br.com.fatecempregos.security;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

public class FatecEmpregosAuthenticationProvider extends
		DaoAuthenticationProvider {

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		FatecEmpregosUserDetails fatecUserDetails = (FatecEmpregosUserDetails) userDetails;
		if (fatecUserDetails.getUser() == null) {
			throw new BadCredentialsException("Usuário inexistente!");
		}
		String currentPassword = authentication.getCredentials().toString();
		if (!fatecUserDetails.getPassword().equals(currentPassword)) {
			throw new BadCredentialsException("Senha inválida!");
		}
		super.additionalAuthenticationChecks(userDetails, authentication);
	}
}
