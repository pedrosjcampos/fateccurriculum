package br.com.fatecempregos.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import br.com.fatecempregos.model.Usuario;
import br.com.fatecempregos.predicate.UsuarioPredicate;
import br.com.fatecempregos.service.UsuarioService;

@Component("userDetailService")
public class FatecEmpregosUserDetailsService implements
		org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	private UsuarioService usuarioService;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		Usuario user = usuarioService.findRow(UsuarioPredicate
				.buscarUsuarioPorLogin(username));
		FatecEmpregosUserDetails userDetails = new FatecEmpregosUserDetails(
				user);
		return userDetails;
	}

}
