/**
 * 
 */
package br.com.fatecempregos.config.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import br.com.fatecempregos.config.core.ApplicationContext;

/**
 * Contexto de configuração do projeto web
 *
 * @author tiarebalbi
 * @date Apr 18, 2013
 * @package br.com.fatecempregos.configuracao
 */
@Configuration
@PropertySource({"classpath:application-web.properties"})
@ComponentScan(basePackages = {
		"br.com.fatecempregos.controller",
		"br.com.fatecempregos.security"
})
@ImportResource(value={"classpath:spring-*-context.xml"})
@EnableWebMvc
@Import(value={ApplicationContext.class})
public class ApplicationWebContext extends WebMvcConfigurerAdapter {
	
	private static final String VIEW_RESOLVER_PREFIX = "/WEB-INF/view/";
    private static final String VIEW_RESOLVER_SUFFIX = ".jsp";
	private static final String PROPRIEDADE_NOME_MESSAGESOURCE_BASENAME = "message.source.basename";
    private static final String PROPRIEDADE_NOME_MESSAGESOURCE_USE_CODE_AS_DEFAULT_MESSAGE = "message.source.use.code.as.default.message";

    @Resource
    private Environment environment;
    
    
    /**
     * configurando os repositorios web disponivel para visualização
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/assets/**").addResourceLocations("/resources/assets/");
    }

    /**
     * Ativando o redirecionamento padrão
     * @param configurer
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
	
	
	 /**
     * Todos os tipos padrões de exception você pode configurar aqui ou criar o seu método de exception handler
     * @return SimpleMappingExceptionResolver
     */
    @Bean
    public SimpleMappingExceptionResolver exceptionResolver() {
        SimpleMappingExceptionResolver exceptionResolver = new SimpleMappingExceptionResolver();

        Properties exceptionMappings = new Properties();

        //TODO Criar método de acesso error/error ou relativo ao erro
        exceptionMappings.put("java.lang.Exception", "error/error");
        exceptionMappings.put("java.lang.RuntimeException", "error/error");

        exceptionResolver.setExceptionMappings(exceptionMappings);

        Properties statusCodes = new Properties();

        //TODO Criar mapeamento das páginas abaixo
        statusCodes.put("error/404", "404");
        statusCodes.put("error/error", "500");

        exceptionResolver.setStatusCodes(statusCodes);

        return exceptionResolver;
    }

    /**
     * Configuração do {@link MessageSource} bean
     * @return MessageSource
     */
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        
        messageSource.setBasename(environment.getRequiredProperty(PROPRIEDADE_NOME_MESSAGESOURCE_BASENAME));
        messageSource.setUseCodeAsDefaultMessage(Boolean.parseBoolean(environment.getRequiredProperty(PROPRIEDADE_NOME_MESSAGESOURCE_USE_CODE_AS_DEFAULT_MESSAGE)));

        return messageSource;
    }

    /**
     * {@link ViewResolver}
     * @return ViewResolver
     */
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix(VIEW_RESOLVER_PREFIX);
        viewResolver.setSuffix(VIEW_RESOLVER_SUFFIX);

        return viewResolver;
    }
    
    /**
     * @return {@link TilesConfigurer}
     */
    @Bean
	public TilesConfigurer configuracaoTiles () {
		TilesConfigurer config = new TilesConfigurer();
		String[] definicoes = new String[2];
		definicoes[0] = "/WEB-INF/views/tiles.xml";
		definicoes[1] = "/WEB-INF/**/tiles-*.xml";
		
		config.setDefinitions(definicoes);
		return config;
	}
    /**
     * @return {@link TilesViewResolver}
     */
    @Bean
	public TilesViewResolver tileViewResolver() {
		TilesViewResolver tiles = new TilesViewResolver();
		return tiles;
	}
    /**
     * Jackson
	 * @return ObjectMapper
	 */
	@Bean
	public ObjectMapper objectMapper() {
	    org.codehaus.jackson.map.ObjectMapper objectMapper = new org.codehaus.jackson.map.ObjectMapper();
	    return objectMapper;
	}
	
	/**
	 * @return MappingJacksonJsonView
	 */
	@Bean
	public MappingJacksonJsonView mappingJacksonJsonView() {
	    MappingJacksonJsonView mappingJacksonJsonView = new MappingJacksonJsonView();
	    mappingJacksonJsonView.setContentType("application/json");
	    mappingJacksonJsonView.setObjectMapper(this.objectMapper());
	    mappingJacksonJsonView.setEncoding(JsonEncoding.UTF8);
	    mappingJacksonJsonView.setPrefixJson(false);
	    return mappingJacksonJsonView;
	}

	/**
	 * @return ContentNegotiatingViewResolver
	 */
	@Bean
	public ContentNegotiatingViewResolver contentNegotiatingViewResolver() {
	    List<ViewResolver> viewResolvers = new ArrayList<ViewResolver>();
	    
	    // Tiles
	    viewResolvers.add(this.tileViewResolver());
	    viewResolvers.add(this.viewResolver());
	    
	    // Views 
	    List<View> defaultViews = new ArrayList<View>();
	    defaultViews.add(this.mappingJacksonJsonView());
	    
	    
	    ContentNegotiatingViewResolver contentNegotiatingViewResolver = new ContentNegotiatingViewResolver();
	    
		contentNegotiatingViewResolver.setViewResolvers(viewResolvers);
		contentNegotiatingViewResolver.setDefaultViews(defaultViews);
		contentNegotiatingViewResolver.setOrder(0);
		
	    return contentNegotiatingViewResolver;
	}

}
