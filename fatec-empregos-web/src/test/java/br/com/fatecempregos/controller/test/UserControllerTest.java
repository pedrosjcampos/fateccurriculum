package br.com.fatecempregos.controller.test;

import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.view;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.server.MockMvc;
import org.springframework.test.web.server.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.fatecempregos.config.web.test.ApplicationWebContextTest;
import br.com.fatecempregos.config.web.test.WebContextLoader;
import br.com.fatecempregos.model.Usuario;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = WebContextLoader.class, classes = { ApplicationWebContextTest.class })
public class UserControllerTest {

	@Resource
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;
	

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.annotationConfigSetup(
				ApplicationWebContextTest.class).build();
	}

	@Test
	public void index() throws Exception {
		this.mockMvc.perform(get("/usuarios")).andExpect(status().isOk())
				.andExpect(view().name("usuarios/index"));
	};

}
