package br.com.fatecempregos.config.web.test;


public class WebContextLoader extends GenericWebContextLoader {
	
	public WebContextLoader() {
		super("src/main/webapp/WEB-INF/", false);
	}
}
