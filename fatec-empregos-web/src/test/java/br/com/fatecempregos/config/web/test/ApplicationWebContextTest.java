package br.com.fatecempregos.config.web.test;

import java.util.Properties;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import com.jolbox.bonecp.BoneCPDataSource;

@Configuration
@PropertySource({ "classpath:application-web.properties", "classpath:definicoes-test.properties" })
@ComponentScan(basePackages = { "br.com.fatecempregos.controller",
								"br.com.fatecempregos.security",
								"br.com.fatecempregos.component",
								"br.com.fatecempregos.service" })
@EnableWebMvc
@EnableTransactionManagement
@EnableJpaRepositories("br.com.fatecempregos.repository")
@ImportResource(value="classpath:spring-security-context.xml")
public class ApplicationWebContextTest extends WebMvcConfigurerAdapter {

	@Resource
	private Environment environment;

	private static final String PROPRIEDADE_NOME_MESSAGESOURCE_BASENAME = "message.source.basename";
	private static final String PROPRIEDADE_NOME_MESSAGESOURCE_USE_CODE_AS_DEFAULT_MESSAGE = "message.source.use.code.as.default.message";
	private static final String NOME_PROPRIEDADE_DATABASE_DRIVER = "db.driver";
    private static final String NOME_PROPRIEDADE_DATABASE_PASSWORD = "db.password";
    private static final String NOME_PROPRIEDADE_DATABASE_URL = "db.url";
    private static final String NOME_PROPRIEDADE_DATABASE_USERNAME = "db.username";

    private static final String NOME_PROPRIEDADE_HIBERNATE_DIALECT = "hibernate.dialect";
    private static final String NOME_PROPRIEDADE_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    private static final String NOME_PROPRIEDADE_HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";
    private static final String NOME_PROPRIEDADE_HIBERNATE_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";
    private static final String NOME_PROPRIEDADE_HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private static final String NOME_PROPRIEDADE_ENTITYMANAGER_PACKAGES_TO_SCAN = "entitymanager.packages.to.scan";
	private static final String NOME_PROPRIEDADE_HIBERNATE_CACHE = "hibernate.cache.provider_class";
	
	/**
	 * Método para configuração do pool de conexão (BoneCP)
	 * 
	 * @return {@link DataSource}
	 */
	@Bean
    public DataSource dataSource() {
        BoneCPDataSource dataSource = new BoneCPDataSource();

        dataSource.setDriverClass(environment.getRequiredProperty(NOME_PROPRIEDADE_DATABASE_DRIVER));
        dataSource.setJdbcUrl(environment.getRequiredProperty(NOME_PROPRIEDADE_DATABASE_URL));
        dataSource.setUsername(environment.getRequiredProperty(NOME_PROPRIEDADE_DATABASE_USERNAME));
        dataSource.setPassword(environment.getRequiredProperty(NOME_PROPRIEDADE_DATABASE_PASSWORD));
        
        return dataSource;
    }
	
    /**
     * Gerenciador de Transações
     * 
     * @return {@link JpaTransactionManager}
     * @throws ClassNotFoundException
     */
    @Bean
    public JpaTransactionManager transactionManager() throws ClassNotFoundException {
        JpaTransactionManager transactionManager = new JpaTransactionManager();

        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }
    
    
    /**
     * Configuração do Entity Manager
     * @return {@link LocalContainerEntityManagerFactoryBean}
     * @throws ClassNotFoundException
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws ClassNotFoundException {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();

        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan(environment.getRequiredProperty(NOME_PROPRIEDADE_ENTITYMANAGER_PACKAGES_TO_SCAN));

        Properties jpaProterties = new Properties();
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_DIALECT, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_DIALECT));
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_FORMAT_SQL, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_FORMAT_SQL));
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_HBM2DDL_AUTO, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_HBM2DDL_AUTO));
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_NAMING_STRATEGY, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_NAMING_STRATEGY));
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_SHOW_SQL, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_SHOW_SQL));
        jpaProterties.put(NOME_PROPRIEDADE_HIBERNATE_CACHE, environment.getRequiredProperty(NOME_PROPRIEDADE_HIBERNATE_CACHE));

        entityManagerFactoryBean.setJpaProperties(jpaProterties);

        return entityManagerFactoryBean;
    }

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/assets/**").addResourceLocations(
				"/resources/assets/");
	}

	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();

		messageSource.setBasename(environment
				.getRequiredProperty(PROPRIEDADE_NOME_MESSAGESOURCE_BASENAME));
		messageSource
				.setUseCodeAsDefaultMessage(Boolean.parseBoolean(environment
						.getRequiredProperty(PROPRIEDADE_NOME_MESSAGESOURCE_USE_CODE_AS_DEFAULT_MESSAGE)));

		return messageSource;
	}

	@Bean
	public ObjectMapper objectMapper() {
		org.codehaus.jackson.map.ObjectMapper objectMapper = new org.codehaus.jackson.map.ObjectMapper();
		return objectMapper;
	}

	@Bean
	public MappingJacksonJsonView mappingJacksonJsonView() {
		MappingJacksonJsonView mappingJacksonJsonView = new MappingJacksonJsonView();
		mappingJacksonJsonView.setContentType("application/json");
		mappingJacksonJsonView.setObjectMapper(this.objectMapper());
		mappingJacksonJsonView.setEncoding(JsonEncoding.UTF8);
		mappingJacksonJsonView.setPrefixJson(false);
		return mappingJacksonJsonView;
	}
}
