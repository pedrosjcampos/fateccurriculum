package br.com.fatecempregos.security.test;

import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.redirectedUrl;

import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.server.MockMvc;
import org.springframework.test.web.server.MvcResult;
import org.springframework.test.web.server.ResultMatcher;
import org.springframework.test.web.server.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.fatecempregos.common.FatecEmpregosException;
import br.com.fatecempregos.config.web.test.ApplicationWebContextTest;
import br.com.fatecempregos.config.web.test.WebContextLoader;
import br.com.fatecempregos.model.Usuario;
import br.com.fatecempregos.service.UsuarioService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = WebContextLoader.class, classes=ApplicationWebContextTest.class)
public class SpringSecurityTest {

	@Autowired
	private FilterChainProxy springSecurityFilterChain;
	
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;
	
	private Usuario usuario;
	
	private static String SEC_CONTEXT_ATTR = HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;
	
	@Before
	public void setup() throws FatecEmpregosException {
		saveUser();
		this.mockMvc = MockMvcBuilders.webApplicationContextSetup(this.wac)
				.addFilter(this.springSecurityFilterChain).build();
	}

	@After
	public void after(){
		usuarioService.delete(this.usuario.getId());
	}

	@Test
	public void requiresAuthentication() throws Exception {
		mockMvc.perform(get("/home")).andExpect(
				redirectedUrl("http://localhost/login"));
	}
	
	@Test
	public void redirectToLogin() throws Exception {
		mockMvc.perform(get("http://localhost/j_spring_login")).andExpect(
				redirectedUrl("http://localhost/login"));
	}
	
	@Test
	public void successfulAuthentication() throws Exception {
		final String userName = this.usuario.getUsername();
		mockMvc.perform(post("/authentication/login").param("username", userName).param("password", this.usuario.getSenha()))
			.andExpect(redirectedUrl("/home"))
			.andExpect(new ResultMatcher() {
				public void match(MvcResult mvcResult) throws Exception {
					HttpSession session = mvcResult.getRequest().getSession();
					SecurityContext securityContext = (SecurityContext) session.getAttribute(SEC_CONTEXT_ATTR);
					Assert.assertEquals("dancpv", securityContext.getAuthentication().getName());
					Assert.assertNotNull(securityContext);
				}
			});
	}
	
	@Test
	public void failurePasswordAuthentication() throws Exception {
		mockMvc.perform(post("/authentication/login").param("username", this.usuario.getUsername()).param("password", "invalido"))
			.andExpect(redirectedUrl("/login"))
			.andExpect(new ResultMatcher() {
				public void match(MvcResult mvcResult) throws Exception {
					HttpSession session = mvcResult.getRequest().getSession();
					SecurityContext securityContext = (SecurityContext) session.getAttribute(SEC_CONTEXT_ATTR);
					Exception exception = (Exception) session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
					Assert.assertEquals("Senha inválida!", exception.getMessage());
					Assert.assertNull(securityContext);
				}
			});
	}
	
	@Test
	public void failureUserNameAuthentication() throws Exception {
		mockMvc.perform(post("/authentication/login").param("username", "invalido").param("password", this.usuario.getSenha()))
			.andExpect(redirectedUrl("/login"))
			.andExpect(new ResultMatcher() {
				public void match(MvcResult mvcResult) throws Exception {
					HttpSession session = mvcResult.getRequest().getSession();
					SecurityContext securityContext = (SecurityContext) session.getAttribute(SEC_CONTEXT_ATTR);
					Exception exception = (Exception) session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
					Assert.assertEquals("Usuário inexistente!", exception.getMessage());
					Assert.assertNull(securityContext);
				}
			});
	}
	
	private void saveUser() throws FatecEmpregosException {
		this.usuario = new Usuario();
		usuario.setUsername("dancpv");
		usuario.setSenha("dancpv");
		usuarioService.save(usuario);
	}
}
